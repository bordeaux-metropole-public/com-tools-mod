<?php

namespace Drupal\webformfiledw\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\file\Entity\File;

/**
 *
 */
class WebformAttachmentsArchive extends ControllerBase {

  /**
   *
   */
  private function removeAccents($text) {
    $alphabet = [
      'Š' => 'S',
      'š' => 's',
      'Ð' => 'Dj',
      'Ž' => 'Z',
      'ž' => 'z',
      'À' => 'A',
      'Á' => 'A',
      'Â' => 'A',
      'Ã' => 'A',
      'Ä' => 'A',
      'Å' => 'A',
      'Æ' => 'A',
      'Ç' => 'C',
      'È' => 'E',
      'É' => 'E',
      'Ê' => 'E',
      'Ë' => 'E',
      'Ì' => 'I',
      'Í' => 'I',
      'Î' => 'I',
      'Ï' => 'I',
      'Ñ' => 'N',
      'Ò' => 'O',
      'Ó' => 'O',
      'Ô' => 'O',
      'Õ' => 'O',
      'Ö' => 'O',
      'Ø' => 'O',
      'Ù' => 'U',
      'Ú' => 'U',
      'Û' => 'U',
      'Ü' => 'U',
      'Ý' => 'Y',
      'Þ' => 'B',
      'ß' => 'Ss',
      'à' => 'a',
      'á' => 'a',
      'â' => 'a',
      'ã' => 'a',
      'ä' => 'a',
      'å' => 'a',
      'æ' => 'a',
      'ç' => 'c',
      'è' => 'e',
      'é' => 'e',
      'ê' => 'e',
      'ë' => 'e',
      'ì' => 'i',
      'í' => 'i',
      'î' => 'i',
      'ï' => 'i',
      'ð' => 'o',
      'ñ' => 'n',
      'ò' => 'o',
      'ó' => 'o',
      'ô' => 'o',
      'õ' => 'o',
      'ö' => 'o',
      'ø' => 'o',
      'ù' => 'u',
      'ú' => 'u',
      'û' => 'u',
      'ý' => 'y',
      'þ' => 'b',
      'ÿ' => 'y',
      'ƒ' => 'f',
    ];
    $text = strtr($text, $alphabet);
    // Replace all non letters or digits by "_".
    $text = preg_replace('/\W+/', '', $text);
    return $text;
  }

  /**
   *
   */
  private function addSubmissionAttachmentsToZip($submission, $zip_archive): bool {
    /** @var \Drupal\webform\Entity\Webform $webform */
    $webform = $submission->getWebform();
    $webform_file_fields = $webform->getElementsManagedFiles();
    $submission_data = $submission->getData();
    $has_attachments = FALSE;
    foreach ($webform_file_fields as $fileField) {
      if (!empty($submission_data[$fileField])) {
        $file = File::load($submission_data[$fileField]);
        if (!empty($file)) {
          $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager')->getViaUri($file->getFileUri());
          $file_path = $stream_wrapper_manager->realpath();
          if (file_exists($file_path) && is_readable($file_path)) {
            $has_attachments = TRUE;
            $path_parts = pathinfo($file_path);
            $field_name = $submission->getWebform()->getElementDecoded($fileField)['#title'];
            $field_name = $this->removeAccents($field_name);

            $zip_archive->addFile(
              $file_path, $submission->id() . '_' . $field_name . '.' . $path_parts['extension']);
          }
          else {
            \Drupal::logger('webformfiledw')->warning(
              t('File @file missing or not readable.', ['@file' => $file_path]));
          }
        }
        else {
          \Drupal::logger('webformfiledw')->warning(
            t('File with id @id not found.',
              ['@id' => $submission_data[$fileField]]));
        }
      }
    }
    return $has_attachments;
  }

  /**
   *
   */
  private function buildAttachmentsArchive($sid) {

    $submission = WebformSubmission::load($sid);
    if (empty($submission)) {
      \Drupal::logger('webformfiledw')->warning(
        t('Webform submission @id not found.', ['@id' => $sid]));
      return FALSE;
    }

    $temp_dir = \Drupal::service('file_system')->getTempDirectory();
    $path_archive = $temp_dir . DIRECTORY_SEPARATOR . $sid . 'webformfiledw.zip';
    $zip_archive = new \ZipArchive();
    $zip_archive->open($path_archive, \ZipArchive::CREATE);

    $has_attachments =
      $this->addSubmissionAttachmentsToZip($submission, $zip_archive);

    $zip_archive->close();

    if ($has_attachments) {
      header("Cache-Control: private, must-revalidate");
      header("Content-Transfer-Encoding: binary");
      header('Content-Disposition: attachment;filename=' . $sid . '.zip');
      header('Content-Type: application/zip');
      // header('Content-Length: ' . filesize($pathArchive)); -> Pb avec IE.
      readfile($path_archive);
      unlink($path_archive);
      flush();
      die();
    }
    else {
      return FALSE;
    }
  }

  /**
   *
   */
  public function downloadAttachmentsArchive($sid) {
    if (!$this->buildAttachmentsArchive($sid)) {
      return [
        '#markup' => $this->t('Aucune pièce jointe trouvée pour la soumission @sid.',
          ['@sid' => $sid]),
      ];
    }
  }

  /**
   *
   */
  public function downloadNodeAttachmentsArchive($nid, $sid) {
    if (!$this->buildAttachmentsArchive($sid)) {
      return [
        '#markup' => $this->t('Aucune pièce jointe trouvée pour la soumission @sid du formulaire associé au contenu @nid.',
          ['@sid' => $sid, '@nid' => $nid]),
      ];
    }
  }

}
