<?php

namespace Drupal\bm_vbo_file_status\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\file\Entity\File;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;

/**
 * Make file permanent.
 *
 * @Action(
 *   id = "views_bulk_operations_bm_file_set_permanent",
 *   label = @Translation("Make file permanent"),
 *   type = "file",
 *   confirm = TRUE,
 * )
 */
class FileSetPermanentAction extends ViewsBulkOperationsActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    if ($entity instanceof File) {
      $entity->setPermanent();
      $entity->save();
      return $this->t('Set files permanent');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = $account->hasPermission('delete files')
      ? AccessResult::allowed() : $object->access('update', $account, TRUE);
    return $return_as_object ? $result : $result->isAllowed();
  }

}
