<?php

namespace Drupal\bm_vbo_file_status\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\file\Entity\File;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;

/**
 * Make file temporary.
 *
 * @Action(
 *   id = "views_bulk_operations_bm_file_set_temporary",
 *   label = @Translation("Make file temporary"),
 *   type = "file",
 *   confirm = TRUE,
 * )
 */
class FileSetTemporaryAction extends ViewsBulkOperationsActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    if ($entity instanceof File) {
      // Mark the file for removal by file_cron().
      $entity->setTemporary();
      $entity->save();
      return $this->t('Set files temporary');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = $account->hasPermission('delete files')
      ? AccessResult::allowed() : $object->access('update', $account, TRUE);
    return $return_as_object ? $result : $result->isAllowed();
  }

}
