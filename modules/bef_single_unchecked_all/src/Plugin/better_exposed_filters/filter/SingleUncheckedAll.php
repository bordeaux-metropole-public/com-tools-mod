<?php

namespace Drupal\bef_single_unchecked_all\Plugin\better_exposed_filters\filter;

use Drupal\better_exposed_filters\Plugin\better_exposed_filters\filter\Single;
use Drupal\Core\Form\FormStateInterface;

/**
 * Single on/off widget implementation which defaults to all if unchecked.
 *
 * @BetterExposedFiltersFilterWidget(
 *   id = "bef_single_unchecked_all",
 *   label = @Translation("Single On/Off Checkbox (Unchecked for all)"),
 * )
 */
class SingleUncheckedAll extends Single {

  /**
   * {@inheritdoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $form_state) {

    parent::exposedFormAlter($form, $form_state);

    $field_id = $this->getExposedFilterFieldId();
    if (isset($form[$field_id]) && $form[$field_id]['#value'] === 0) {
      $form[$field_id]['#value'] = '';
    }
  }

}
