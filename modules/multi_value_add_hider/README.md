# Multi Value Add Hider

This module prevents the default empty add form from appearing when editing an
unlimited multivalued field, except when the field has no existing values.

It helps keep the edit form clean and reduces unnecessary UI clutter while still
allowing new values to be added when needed.
